# README #

### What is this repository for? ###

* AlbertaSat's software for the cube satellites on board computer (OBC)

### How do I get set up? ###

* Add your name to the contributors.txt file in this repo
* Check out an LPCexpresso board in the club room or use your own
* Download LPCexpresso eclipse
* Refer to the AlbertaSat wiki for bugs encountered (http://albertasat.wikispaces.com)

### Contribution guidelines ###

* guidelines are still being decided on.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact